data "alicloud_images" "first" {
  name_regex  = "^ubuntu"
  most_recent = true
  owners      = "system"
}

data "alicloud_instance_types" "instance_2c4g" {
  cpu_core_count = 2
  memory_size = 4
}

# Create a web server
resource "alicloud_instance" "web01" {
  image_id             = "${data.alicloud_images.first.images.0.id}"
  internet_charge_type = "PayByTraffic"
  instance_type        = "${data.alicloud_instance_types.instance_2c4g.instance_types.0.id}"
  system_disk_category = "cloud_efficiency"
  security_groups      = ["${alicloud_security_group.default.id}"]
  instance_name        = "web01"
  vswitch_id           = "${alicloud_vswitch.vsw.id}"
  password             = "${var.web01_user_instance_password}"
  internet_max_bandwidth_out = 1
}
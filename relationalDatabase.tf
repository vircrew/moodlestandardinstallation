resource "alicloud_db_instance" "master" {
    engine = "MySQL"
    engine_version = "5.7"
    instance_type = "rds.mysql.t1.small"
    instance_storage = "20"
    vswitch_id = "${alicloud_vswitch.vsw.id}"
    security_ips = ["0.0.0.0/0"]

}
resource "alicloud_db_database" "default" {
    instance_id = "${alicloud_db_instance.master.id}"
    name = "moodle_database"
    character_set = "utf8"
}
resource "alicloud_db_connection" "default" {
    instance_id = "${alicloud_db_instance.master.id}"
    connection_prefix = "alicloud"
    port = "3306"
}
resource "alicloud_db_account" "default" {
    instance_id = "${alicloud_db_instance.master.id}"
    name = "${var.db_user}"
    password = "${var.db_password}"
}
resource "alicloud_db_account_privilege" "default" {
    instance_id = "${alicloud_db_instance.master.id}"
    account_name = "${alicloud_db_account.default.name}"
    privilege = "ReadWrite"
    db_names = ["${alicloud_db_database.default.name}"]
}
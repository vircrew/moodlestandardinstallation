variable "access_key" {
  type = "string"
}

variable "secret_key" {
  type = "string"
}

variable "region" {
    type="string"
    default="ap-southeast-2" 
}
variable "switch_region" {
    type="string"
    default="ap-southeast-2a"
}

variable "db_user" {
    type = "string"
}
variable "db_password" {
  type ="string"
}

variable "vpc_name" {
  type = "string"
  default = "default"
}

variable "security_group_name" {
  type = "string"
  default = "default"
}
//Password to an instance is a string of 8 to 30 characters. It must contain uppercase/lowercase letters and numerals, but cannot contain special symbols.
variable "web01_user_instance_password" {
  type = "string"
  default = "Admin123"
}
**Moodle Standard Installation**
# Basic Requirements
1. A working web server (e.g. Apache)
2. PHP configured with related extensions
3. A database (e.g. MySQL, MariaDB or PostgreSQL)

# Prerequisite
## Alibaba Cloud Access & Secret Keys
Required to access your resources using API. You can create and manage all the AccessKeys on the [Access Key Management page](https://ak-console.aliyun.com/?spm=a2c63.p38356.879954.7.3cc69760UaC3CV#/accesskey) in the Alibaba Cloud console.

